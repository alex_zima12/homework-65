import React, {useEffect, useState} from 'react';
import axiosPages from "../../axiosPages";

const Page = props => {
    const [pages, setPages] = useState(null);
    useEffect(() => {
        const getPagesList = async () => {
            const pagesResponse = await axiosPages.get('pages/' + props.match.params.name + '.json');
            setPages(pagesResponse.data);
        };
        getPagesList().catch(console.error);
    }, [props.match.params.name]);

    return pages && (
        <div className="container">
            <div className="card-body">
                <h1>{pages.title}</h1>
                <p className="card-title">{pages.content}</p>
            </div>
        </div>

    );
};

export default Page;