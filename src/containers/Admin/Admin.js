import React, {useEffect, useState} from 'react';
import axiosPages from "../../axiosPages";

const PAGES = [
    {title: 'about'},
    {title: 'competition'},
    {title: 'contacts'},
    {title: 'home'},
    {title: 'popular'},
    {title: 'vacancy'}
]

const Admin = props => {
    const [newTitle, SetNewTitle] = useState('');
    const [newContent, SetNewContent] = useState('');
    const [newPage, SetNewPage] = useState('about');
    const [loading, setLoading] = useState(true);

    const changePage = event => {
        SetNewPage(event.target.value);
    };

    useEffect(() => {
        const getPagesList = async () => {
            setLoading(true);
            const pageResponse = await axiosPages.get('pages/' + newPage + '.json');
            SetNewTitle(pageResponse.data.title);
            SetNewContent(pageResponse.data.content);

        };
        getPagesList().finally(() => setLoading(false)).catch(console.error);
    }, [newPage]);

    const changeTitle = event => {
        SetNewTitle(event.target.value);
    };

    const changeContent = event => {
        SetNewContent(event.target.value);
    };

    const changeInformation = async (e, name) => {
        e.preventDefault();
        await axiosPages.put('pages/' + newPage + '.json', {
            title: newTitle,
            content: newContent,
        });
        props.history.push('pages/'+ newPage);
    };

    if (loading) {
        return (
            <div className="text-center mt-5">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );
    }
    ;

    return (
        <form className="form-group my-5 container">
            <div className="form-group">
                <label htmlFor="exampleFormControlSelect1">Pages</label>
                <select
                    className="form-control"
                    id="exampleFormControlSelect1"
                    onChange={changePage}
                    value={newPage}
                >
                    <option value={PAGES[0].title}>{PAGES[0].title}</option>
                    <option value={PAGES[1].title}>{PAGES[1].title}</option>
                    <option value={PAGES[2].title}>{PAGES[2].title}</option>
                    <option value={PAGES[3].title}>{PAGES[3].title}</option>
                    <option value={PAGES[4].title}>{PAGES[4].title}</option>
                </select>
            </div>
            <div className="form-group">
                <label>Title</label>
                <input
                    value={newTitle}
                    type="text"
                    name="author"
                    className="form-control"
                    onChange={changeTitle}
                    required
                />
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlTextarea1">Content</label>
                <textarea
                    value={newContent}
                    className="form-control"
                    name="text"
                    id="exampleFormControlTextarea1"
                    rows="3"
                    onChange={changeContent}
                    required
                />
            </div>
            <button
                type="submit"
                className="btn btn-primary"
                onClick={(event) => changeInformation(event, props.match.params.name)}
            >Save
            </button>
        </form>
    );
};

export default Admin;