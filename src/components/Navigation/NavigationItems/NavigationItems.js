import React from 'react';
import './NavigationItems.css';
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <ul className="NavigationItems">
            <NavigationItem to="/pages/home"  >Home</NavigationItem>
            <NavigationItem to="/pages/about"  >About Us</NavigationItem>
            <NavigationItem to="/pages/competition">Competition</NavigationItem>
            <NavigationItem to="/pages/popular" >Popular</NavigationItem>
            <NavigationItem to="/pages/vacancy" >Vacancy</NavigationItem>
            <NavigationItem to="/pages/contacts" >Contacts</NavigationItem>
            <NavigationItem to="/admin" >Admin</NavigationItem>
        </ul>
    );
};

export default NavigationItems;