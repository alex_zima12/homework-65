import axios from 'axios';

const axiosPages = axios.create({
    baseURL: 'https://jsgroup7exam.firebaseio.com/'
});

export default axiosPages;