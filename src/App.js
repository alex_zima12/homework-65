import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import Page from "./containers/Page/Page";
import Admin from "./containers/Admin/Admin";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Page}/>
            <Route path="/admin" component={Admin}/>
            <Route path="/pages/:name" component={Page}/>
            <Route render={() => <h1>404 Not Found</h1>}/>
        </Switch>
    </Layout>
);

export default App;